package my.startup.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Describe awesome user idea
 * @author alek
 */
@Data
@Entity
@Table(name = "idea")
public class Idea {

    /** Describe identifier in database */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /** Describe ideas name */
    @Column(name = "name")
    private String name;

    /** Describe date where user realize idea */
    @Column(name = "realizeDate")
    private LocalDate dateOfRealization;

    /** Describe date of idea creation */
    @Column(name = "createDate")
    private LocalDate dateOfCreation;

    /** Describe color of idea (must be)
     * red - bad idea realization
     * yellow - so so
     * green - all good
     */
    @Column(name = "color")
    private String color;

    /** Describe users impressions */
    @Column(name = "impression")
    private String impressions;

}
