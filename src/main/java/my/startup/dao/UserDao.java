package my.startup.dao;

import my.startup.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Dao object for getting information for db users.
 * @see User to more information about table structure
 * @author alek
 * @since 0.1
 */
@Transactional
public interface UserDao extends JpaRepository<User, Integer> {
}
