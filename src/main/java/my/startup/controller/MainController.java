package my.startup.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for main page
 * @author alek
 * @since 0.1
 */
@Controller
public class MainController {

    /**
     * Add mapping for / on index.html
     * @return String with main page name
     * */
    @RequestMapping("/")
    public String index() {
        return "index";
    }

}
