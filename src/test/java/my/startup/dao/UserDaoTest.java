package my.startup.dao;

import my.startup.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Class for testing right work of dao with database
 * @author alek
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Before
    public void init() {
        User user = new User();
        user.setId(1);
        user.setName("Name");
        user.setSurname("Surname");
        user.setEmail("user@us.er");
        user.setDob("11/11/1992");
        user.setLogin("userss");

        userDao.save(user);

        User user1 = new User();
        user.setId(2);
        user.setName("Name1");
        user.setSurname("Surname1");
        user.setEmail("user@us.er");
        user.setDob("11/11/1992");
        user.setLogin("userss");

        userDao.save(user1);

        User user2 = new User();
        user.setId(3);
        user.setName("Name2");
        user.setSurname("Surname2");
        user.setEmail("user@us.er");
        user.setDob("11/11/1992");
        user.setLogin("userss");

        userDao.save(user2);
    }

    @Test
    public void saveUser() {
        User user = new User();
        user.setId(4);
        user.setName("Nameq");
        user.setSurname("Surnameq");
        user.setEmail("user@us.er");
        user.setDob("11/11/1992");
        user.setLogin("userss");

        userDao.save(user);

        User user1 = userDao.getOne(4);
        assertThat(user1.getName().equals(user.getName()));
    }

    @Test
    public void deleteUser() {
        User user = userDao.getOne(1);

        userDao.delete(user);

        assertThat(userDao.getOne(1) == null);
    }

    @Test
    public void updateUser() {
        User user = userDao.getOne(2);
        user.setName("Test");
        userDao.save(user);

        assertThat(userDao.getOne(2).getName().equals("Test"));
    }



}
